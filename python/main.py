import json
import socket
import sys
import math

#last_pos = 0

class NoobBot(object):
    
    switch_counter = 0
    
    last_switch = "Left"
    
    turbo_on = 0
    
    last_angle = 0
    
    last_read_line = ""
    track_data = {}
    crash_data = []
    last_known_position = {}
    current_throttle = 0.8
    crashed_pieces = []
    current_speed = 0
    
    current_angle = 0
    
    last_reported_piece = ""
    
    turbo_available = 0
    
    turbo_message_sent = 0
    
    #turbo_used = 0
    
    turbo_able_pieces = []

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        self.current_throttle = throttle
    
    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
    	
    	
    	#	GET SPEED
    	try:
	    	if self.last_known_position["piece_position"] > 0 and self.last_known_position["piece_position"] < 20:
	    		self.current_speed = data[0]["piecePosition"]["inPieceDistance"] - self.last_known_position["piece_position"]
    	except:
    		pass
    	
    	#	UPDATE POSITION
        self.last_known_position["piece"] = data[0]["piecePosition"]["pieceIndex"]
        self.last_known_position["piece_position"] = data[0]["piecePosition"]["inPieceDistance"]
        
        if self.last_reported_piece != self.last_known_position["piece"]:
        	self.last_reported_piece = self.last_known_position["piece"]
        	#print self.last_known_position["piece"]

        
        
        #	CHECK IF ON A TURBO PIECE AND TURBO AVAILABLE
        
        
        
        if self.turbo_available == 1 and self.last_known_position["piece"] in self.turbo_able_pieces:
        	if self.current_speed < 10:
	    		self.turbo_available = 0
	    		#print "TURBO STATUS IS NOW " + str(self.turbo_available)
	    		self.msg("turbo", "turbojee!")
    		else:
    			self.current_throttle = 0.0
    			self.throttle(self.current_throttle)
    			
    		#self.turbo_used = 1
    		#self.turbo_available == 0
        
        #	IF NOT IN A POSITION TO USE TURBO DO SOMETHING ELSE
        
        else:

        	#self.throttle(self.current_throttle)
        	
        	self.current_angle = math.sqrt((data[0]["angle"] * data[0]["angle"]))
        	#print(self.current_angle)
        	
        	
        	if self.last_known_position["piece"] + 2 in self.crashed_pieces and self.last_known_position["piece_position"] > 5:
        		#print "JARRUTA AJOISSA!"
        		self.current_throttle = 0.1
        	if self.last_known_position["piece"] + 1 in self.crashed_pieces:
        		#print "JARRUTA!"
        		self.current_throttle = 0.1
        	else:
	        	if self.current_angle > 40:#	50-45 best35best25best8works15worksbetterMUOKKAA TATA ----------------------------------
	        		#	slow down
	        		if self.current_throttle > 0.2:
	        			self.current_throttle = self.current_throttle - 0.2
	        	else:
	        		if self.current_throttle < 0.9:
	        			self.current_throttle = self.current_throttle + 0.1
	        	
	        	
	        	
	        	#print "Throttle: " + str(self.current_throttle) + " Speed: " + str(self.current_speed) + " Angle: " + str(self.current_angle) + " Pos: " + str(self.last_known_position["piece_position"])
        	
        	if self.last_known_position["piece_position"] > 65 and self.current_angle > 1 and self.current_angle < 10:#voiko aloittaa kaasutuksen aikaisemmin?
        		self.current_throttle = 0.6
        	
        	
        	#	IF GETTING CLOSER TO A CURVE WITH RADIUS OVER X CAP SPEED
        	try:
        		if self.track_data["race"]["track"]["pieces"][self.last_known_position["piece"] + 2]["radius"] < 200 and self.current_speed > 10:
        			self.current_throttle = 0.1
        		elif self.track_data["race"]["track"]["pieces"][self.last_known_position["piece"] + 1]["radius"] < 200 and self.current_speed > 5.0:#	6.0BEST5.5best5.0best6.4 6.0betterworks6.5slow
	        		if self.current_throttle > 0.0:
	        			self.current_throttle = 0.1
	        			#print "slowing done in corners"
	        except:
        		pass
        	
        	if self.last_angle > self.current_angle and self.current_angle < 50:#20works
	        		#print "coming down from drift, more speed!"
	        		if self.current_throttle < 0.9:
	        			self.current_throttle = self.current_throttle + 0.1
        		
        	#	while turbo on, press less pedal
        	if self.turbo_on == 1:
        		self.current_throttle = 0.5#0.75 unstable0.7 unstable
        	
        	#print data[0]
        	
        	switch_piece = False
        	
        	self.switch_counter = self.switch_counter - 1
        	if self.switch_counter > 0:
        		pass
        	else:
	        	try:
	        		switch_piece = self.track_data["race"]["track"]["pieces"][self.last_known_position["piece"]]["switch"]
	        		
	        		if switch_piece == True:
	        			print "this is switch piece"
	        			self.switch_counter = 300
	        			if self.last_switch == "Left":
	        				self.last_switch = "Right"
	        				self.msg("switchLane", "Right")
	        			elif self.last_switch == "Right":
	        				self.last_switch = "Left"
	        				print "switch to left"
	        				self.msg("switchLane", "Left")
	        	except:
	        		pass
        	
        	
        	self.last_angle = self.current_angle
        		
        	if switch_piece == True:
        		pass
        	else:
        		self.throttle(self.current_throttle)
        			
        
        
        	'''
	        if self.last_known_position["piece"] + 2 in self.crashed_pieces and self.last_known_position["piece_position"] > 30:
	        	if self.last_known_position["piece_position"] > 35:
	        		self.throttle(0.1)
	        	else:
	        		self.throttle(1.0)
	        
	        elif self.last_known_position["piece"] + 1 in self.crashed_pieces:
	        	#print "Jarruta hyva mies!"
	        	self.throttle(0.1)
	        
	        #elif self.last_known_position["piece"] + 3 in self.crashed_pieces:
	        #	#print "Jarruta hyva mies!"
	        #	self.throttle(0.5)
	        
	        elif self.last_known_position["piece"] in self.crashed_pieces:
	        	
	        	if self.last_known_position["piece_position"] > 63:
	        		self.throttle(0.8)
	        	else:
	        		self.throttle(0.5)
	        	
	        elif self.last_known_position["piece"] - 1 in self.crashed_pieces:
	        	self.throttle(1.0)
	        else:
	        	self.throttle(1.0)
	        
	        
	        '''
	        #print self.last_known_position["piece"] + 1
	        #print self.crashed_pieces
	        
	       
	        #return last_known_position
        #self.turbo_used == 0

    def on_crash(self, data):
        print("Someone crashed")
        
        piece_dict = {}
        
        try:
        	piece_dict["piece_radius"] = self.track_data["race"]["track"]["pieces"][self.last_known_position["piece"]]["radius"]
        except:
        	piece_dict["piece_radius"] = "na"
        
        piece_dict["piece_index"] = self.last_known_position["piece"]
        piece_dict["piece_position"] = self.last_known_position["piece_position"]
        piece_dict["piece_throttle"] = self.current_throttle
        
        self.crashed_pieces.append(self.last_known_position["piece"])
        
        #print self.track_data
        self.crash_data.append(piece_dict)
        
        #print self.crash_data
        
        #print "Crash piece number: " + last_known_position
        #print data
        self.ping()
        #return last_known_position

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
	
	#def on_game_init(data):
	#	print data
	
    def msg_loop(self):
    
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error
            #'gameInit': self.on_game_init
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            #print msg
            
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                
                msg_map[msg_type](data)
                
                #if return_msg is int:
                #	print return_msg
                
                #print return_msg
                
                #if msg_type == crash:
                	
                
            else:
            	self.ping()
                print("Got {0}".format(msg_type))
                
                if msg_type == "spawn":
                	self.throttle(self.current_throttle)
                if msg_type == "turboEnd":
                	self.turbo_on = 0
                if msg_type == "turboStart":
                	self.turbo_on = 1	
                
                if msg_type == "gameInit":
                	#print msg['data']
					self.track_data = msg['data']
					'''
					print
					print self.track_data
					print
					print
					'''
					i = 0
					
					for piece in self.track_data["race"]["track"]["pieces"]:
						#print i
						try:
							if self.track_data["race"]["track"]["pieces"][i]["length"] > 80:
								if self.track_data["race"]["track"]["pieces"][i + 1]["length"] > 80 and self.track_data["race"]["track"]["pieces"][i + 2]["length"] > 80 and self.track_data["race"]["track"]["pieces"][i + 3]["length"] > 80 and self.track_data["race"]["track"]["pieces"][i + 4]["length"] > 80:
									self.turbo_able_pieces.append(i)
						except:
							pass
							
						i += 1
						#print piece
					#self.turbo_able_pieces.append()
					#print self.track_data
					
					#for piece in self.turbo_able_pieces:
					#	print piece
					
					#exit()
					
				
                if msg_type == "turboAvailable":
                	self.turbo_available = 1
                	#print "GOT TURBO"
					#print self.track_data
                
                #self.ping()
            line = socket_file.readline()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
